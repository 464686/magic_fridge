import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:magic_fridge/screens/home_screen.dart';
import 'package:magic_fridge/screens/register_screen.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  Map _userData;

  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: 50,
            ),
            Container(
              height: 200,
              child: Image.asset('images/logo_size_invert.jpg'),
            ),
            SizedBox(
              height: 48,
            ),
            TextField(
              controller: emailController,
              decoration: InputDecoration(
                hintText: 'Enter your email',
                contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(32)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 2),
                  borderRadius: BorderRadius.all(Radius.circular(32)),
                ),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            TextField(
              controller: passwordController,
              obscureText: true,
              obscuringCharacter: '*',
              decoration: InputDecoration(
                hintText: 'Enter your password',
                contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(32)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 2),
                  borderRadius: BorderRadius.all(Radius.circular(32)),
                ),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 60),
              child: Material(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.all(Radius.circular(30)),
                elevation: 5,
                child: MaterialButton(
                  minWidth: 200,
                  height: 42,
                  child: Text(
                    'Log in',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  onPressed: () async {
                    //implement login functionality
                    try {
                      final user = await FirebaseAuth.instance.signInWithEmailAndPassword(
                        email: emailController.text,
                        password: passwordController.text,
                      );
                      
                      if (user != null) {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
                      }
                    } catch (e) {
                      print(e);
                    }
                  },
                ),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    child: Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Theme.of(context).scaffoldBackgroundColor,
                        image: DecorationImage(
                          image: AssetImage('images/google_logo.png'),
                          fit: BoxFit.cover
                        ),
                      ),
                    ),
                    onTap: () async {
                      // await _googleSignIn.signIn();
                      googleSignIn(context);
                      // Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomeScreen()));

                      setState(() {

                      });
                    },
                  ),
                  GestureDetector(
                    child: Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Theme.of(context).scaffoldBackgroundColor,
                        image: DecorationImage(
                            image: AssetImage('images/facebook-icon-preview.png'),
                            fit: BoxFit.cover
                        ),
                      ),
                    ),
                    onTap: () async {
                      final result = await FacebookAuth.i.login(
                          permissions: ["public_profile", "email"]
                      );

                      if (result.status == LoginStatus.success) {
                        final requestData = await FacebookAuth.i.getUserData(
                          fields: "email, name, picture.type(large)",
                        );

                        setState(() {
                          _userData = requestData;
                        });

                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomeScreen()));
                      }
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  Text(
                    'Don\'t have an account yet?',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  // SizedBox(width: 10),
                  TextButton(
                    child: Text(
                      'Register',
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => RegisterScreen()));
                    },
                  ),
                ],
              ),
              // padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            ),
          ],
        ),
      ),
    );
  }


  Future<void> googleSignIn(BuildContext context) async {
    final FirebaseAuth _auth = FirebaseAuth.instance;

    try {
      GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
      GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
      AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );
      if (googleSignInAccount != null) {
        UserCredential userCredential = await _auth.signInWithCredential(credential);
        storeTokenAndData(userCredential);
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomeScreen()));
      }
    } catch (e) {
      final snackBar = SnackBar(content: Text(e.toString()));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  void storeTokenAndData(UserCredential userCredential) async {
    await storage.write(
      key: 'token', value: userCredential.credential.token.toString()
    );
    await storage.write(
      key: 'usercredential', value: userCredential.toString()
    );
  }
}




