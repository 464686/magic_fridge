import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:magic_fridge/screens/recipes_screen.dart';
import 'package:magic_fridge/screens/shopping_list_screen.dart';
import 'package:magic_fridge/widgets/bottom_bar.dart';

class RecipeScreen extends StatefulWidget {
  static const String id = 'RecipeScreen';

  final recipeId;
  final ingredientName;
  final ingredientId;
  RecipeScreen(this.recipeId, this.ingredientName, this.ingredientId);

  @override
  _RecipeScreenState createState() => _RecipeScreenState(recipeId, ingredientName, ingredientId);
}

class _RecipeScreenState extends State<RecipeScreen> {

  CollectionReference _recipes = FirebaseFirestore.instance.collection('recipes');
  CollectionReference _ingredients = FirebaseFirestore.instance.collection('ingredients');
  CollectionReference _shoppingList = FirebaseFirestore.instance.collection('shoppingList');

  final recipeId;
  final ingredientName;
  final ingredientId;

  _RecipeScreenState(this.recipeId, this.ingredientName, this.ingredientId);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            showContent(ingredientName, ingredientId);
          },
        ),
        title: Text(
          'Recipe',
          style: Theme
              .of(context)
              .textTheme
              .headline1,
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.search,
              color: Theme
                  .of(context)
                  .primaryIconTheme
                  .color,
            ),
            onPressed: () {
              _displaySearchDialog(context);
            },
          ),
        ],
      ),
      body: StreamBuilder<DocumentSnapshot>(
        stream: FirebaseFirestore.instance.collection('recipes')
            .doc(recipeId)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Loading'),
            );
          }
          return Column(
            children: [
              SizedBox(height: 20),
              Center(
                child: Text(
                  snapshot.data['name'],
                  style: Theme.of(context).textTheme.headline5,
                  // style: Theme.of(context).textTheme.headline2,
                ),
              ),
              SizedBox(height: 15),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Text(
                  'Cooking time: ' + snapshot.data['cookingTime'].toString(),
                  style: Theme.of(context).textTheme.bodyText1,
                  // style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
              SizedBox(height: 15),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Text(
                  'Ingredients: ' + snapshot.data["ingredients"].toString(),
                  style: Theme.of(context).textTheme.bodyText1,
                  // style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
              SizedBox(height: 15),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Text(
                  snapshot.data["cookingProcedure"],
                  style: Theme.of(context).textTheme.bodyText1,
                  // style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
            ],
          );
        },
      ),
      floatingActionButton: Wrap(
        direction: Axis.horizontal,
        children: [
          // Container(
          //   margin: EdgeInsets.all(10),
          // child: FloatingActionButton(
          //   onPressed: () {
          //     _deleteRecipe(recipeId);
          // Navigator.of(context).pop();
          // ScaffoldMessenger.of(context).showSnackBar(
          //   SnackBar(content: Text('You have successfully deleted recipe.')),
          // );
          // },
          // child: Icon(Icons.delete),
          // ),
          // ),
          Container(
            margin: EdgeInsets.all(10),
            child: FloatingActionButton(
              onPressed: () {
                _displayUpdateDialog(context, recipeId);
                // Navigator.of(context).push(MaterialPageRoute(builder: (context) => UpdateRecipeScreen()));
              },
              child: Icon(Icons.update),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomBar(),
      // bottomNavigationBar: BottomNavBar(_currentIndex),
    );
  }

  void showContent(ingredientName, ingredientId) {
    showDialog(
        context: context,
        barrierDismissible: false,

        builder: (BuildContext context) {
          return new AlertDialog(
            content: new SingleChildScrollView(
              child: new ListBody(
                children: [
                  new Text(
                    'Did you prepare your meal?',
                  ),
                ],
              ),
            ),
            actions: [
              new FlatButton(
                child: new Text(
                  'YES',
                  style: Theme
                      .of(context)
                      .textTheme
                      .headline3,
                ),
                onPressed: () {
                  showChoice(ingredientName, ingredientId);
                },
              ),
              new FlatButton(
                child: new Text(
                  'NO',
                  style: Theme
                      .of(context)
                      .textTheme
                      .headline3,
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          RecipesScreen(ingredientName, ingredientId)));
                },
              ),
            ],
          );
        }
    );
  }

  void showChoice(ingredientName, ingredientId) {
    showDialog(
        context: context,
        barrierDismissible: false,

        builder: (BuildContext context) {
          return new AlertDialog(
            content: new SingleChildScrollView(
              child: new ListBody(
                children: [
                  new Text(
                    'Do you want to add ingredient to your shopping list?',
                  ),
                ],
              ),
            ),
            actions: [
              new FlatButton(
                child: new Text(
                  'YES',
                  style: Theme.of(context).textTheme.headline3,
                ),
                onPressed: () {
                  _deleteIngredient(ingredientId);
                  _createShoppingListItem(ingredientName);
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ShoppingListScreen()));
                },
              ),
              new FlatButton(
                child: new Text(
                  'NO',
                  style: Theme.of(context).textTheme.headline3,
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          RecipesScreen(ingredientName, ingredientId)));
                },
              ),
            ],
          );
        }
    );
  }

  final _formKey = GlobalKey<FormState>();
  String name;

  Future<void> _displaySearchDialog(BuildContext context) {
    TextEditingController _searchController = new TextEditingController();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: _searchController,
                    onChanged: (value) {
                      name = value;
                    },
                    decoration: InputDecoration(
                        hintText: 'What are you looking for?',
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Colors.pink,
                      textColor: Colors.white,
                      child: Text('CANCEL'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Color(0xFF0A0E21),
                      textColor: Colors.white,
                      child: Text('SEARCH'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ],
          );
        }
    );
  }

  Future<void> _deleteRecipe(String recipeId) async {
    await _recipes.doc(recipeId).delete();

    // ScaffoldMessenger.of(context).showSnackBar(
    //   SnackBar(content: Text('You have successfully deleted recipe.')),
    // );
  }

  Future<void> _createShoppingListItem(String name) async {
    await _shoppingList.add({'name': name});

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
          content: Text('Ingredient has been moved to your shopping list.')),
    );
  }

  Future<void> _deleteIngredient(String ingredientId) async {
    await _ingredients.doc(ingredientId).delete();
  }

  Future<void> _displayUpdateDialog(BuildContext context, recipeId) async {
    TextEditingController nameController = new TextEditingController();
    TextEditingController cookingTimeController = new TextEditingController();
    TextEditingController ingredientsController = new TextEditingController();
    TextEditingController cookingProcedureController = new TextEditingController();
    DocumentSnapshot recipe = await FirebaseFirestore.instance.collection(
        'recipes').doc(recipeId).get();
    // FirebaseFirestore.instance.collection('recipes').doc(recipeId).snapshots();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              // key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                      hintText: recipe['name'],
                    ),
                  ),
                  TextField(
                    controller: cookingTimeController,
                    decoration: InputDecoration(
                      hintText: recipe['cookingTime'].toString() + ' minutes',
                    ),
                  ),
                  TextField(
                    controller: ingredientsController,
                    decoration: InputDecoration(
                      hintText: recipe['ingredients'].toString(),
                    ),
                  ),
                  TextField(
                    controller: cookingProcedureController,
                    decoration: InputDecoration(
                      hintText: recipe['cookingProcedure'],
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Colors.pink,
                      textColor: Colors.white,
                      child: Text('CANCEL'),
                      onPressed: () {
                        //back to ingredients screen
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Color(0xFF0A0E21),
                      textColor: Colors.white,
                      child: Text('SAVE'),
                      onPressed: () {
                        _recipes.doc(recipeId).set({
                          'name': nameController.text,
                          'cookingTime': cookingTimeController.text,
                          'ingredients': ingredientsController.text,
                          'cookingProcedure': cookingProcedureController.text,
                        });
                        // addController.clear();
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ],
          );
        }
    );
  }

  Future<void> _createOrUpdate([DocumentSnapshot snapshot]) async {
    String action = 'create';
    if (snapshot != null) {
      action = 'update';
      // _nameController.text = snapshot['name'];
    }

    // _displayAddDialog(context);
  }

}