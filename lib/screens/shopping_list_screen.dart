import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:magic_fridge/widgets/bottom_bar.dart';
import 'package:magic_fridge/widgets/top_app_bar.dart';

class ShoppingListScreen extends StatefulWidget {
  static const String id = 'shopping_list_screen';

  @override
  _ShoppingListScreenState createState() => _ShoppingListScreenState();
}

class _ShoppingListScreenState extends State<ShoppingListScreen> {
  int _currentIndex = 4;
  final TextEditingController _nameController = TextEditingController();

  CollectionReference _shoppingList = FirebaseFirestore.instance.collection(
      'shoppingList');

  Future<void> _createOrUpdate([DocumentSnapshot documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _nameController.text = documentSnapshot['name'];
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: _nameController,
                  decoration: InputDecoration(labelText: 'Name'),
                ),
                SizedBox(
                  height: 20,
                ),
                FlatButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  color: Color(0xFF0A0E21),
                  textColor: Colors.white,
                  onPressed: () async {
                    final String name = _nameController.text;
                    if (name != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _shoppingList.add({"name": name});
                      }
                      if (action == 'update') {
                        // Update the product
                        await _shoppingList
                            .doc(documentSnapshot.id)
                            .update({"name": name});
                      }

                      // Clear the text fields
                      _nameController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  Future<void> _deleteItem(String productId) async {
    await _shoppingList.doc(productId).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('You have successfully deleted a product')));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopAppBar('My Shopping List'),
      body: StreamBuilder(
        stream: _shoppingList.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot = streamSnapshot.data
                    .docs[index];
                return Card(
                  margin: EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(documentSnapshot['name']),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () =>
                                  _deleteItem(documentSnapshot.id)),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
        ),
        hoverColor: Theme
            .of(context)
            .accentColor,
        onPressed: () {
          _displayAddDialog(context);
        },
      ),
      bottomNavigationBar: BottomBar(),
      // bottomNavigationBar: BottomNavBar(_currentIndex),
    );
  }

  final _formKey = GlobalKey<FormState>();
  String name;

  Future<void> _displayAddDialog(BuildContext context) {
    TextEditingController _searchController = new TextEditingController();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: _nameController,
                    onChanged: (value) {
                      name = value;
                    },
                    decoration: InputDecoration(
                        hintText: 'What do you need to buy?'),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Colors.pink,
                      textColor: Colors.white,
                      child: Text('CANCEL'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Color(0xFF0A0E21),
                      textColor: Colors.white,
                      child: Text('SAVE'),
                      onPressed: () async {
                        final String name = _nameController.text;
                        if (name != null) {
                          await _shoppingList.add({"name": name});

                          // Clear the text fields
                          _nameController.text = '';

                          // Hide the bottom sheet
                          Navigator.of(context).pop();
                        }
                      },
                    ),
                  ),
                ],
              ),
            ],
          );
        }
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}