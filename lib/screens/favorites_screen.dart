import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:magic_fridge/screens/recipe_screen.dart';
import 'package:magic_fridge/widgets/bottom_bar.dart';
import 'package:magic_fridge/widgets/top_app_bar.dart';

class FavoriteRecipesScreen extends StatefulWidget {
  static const String id = "FavoriteRecipesScreen";

  final ingredientName;
  FavoriteRecipesScreen(this.ingredientName);

  @override
  // _FavoriteRecipesScreenState createState() => _FavoriteRecipesScreenState();
  _FavoriteRecipesScreenState createState() => _FavoriteRecipesScreenState(ingredientName);
}

class _FavoriteRecipesScreenState extends State<FavoriteRecipesScreen> {

  final ingredientName;
  _FavoriteRecipesScreenState(this.ingredientName);

  CollectionReference _recipes = FirebaseFirestore.instance.collection('recipes');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopAppBar('My favorite recipes'),
      body: Center(
        child: StreamBuilder(
            stream: FirebaseFirestore.instance
                .collection('recipes')
                .where('isFavorite', isEqualTo: true)
                .snapshots(),
            builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: Text('Loading'),
                );
              }

              return ListView(
                children: snapshot.data.docs.map((recipe) {
                  return Card(
                    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                    child: ListTile(
                      title: Text(recipe['name']),
                      subtitle: Text(recipe['cookingTime'].toString() + ' minutes'),
                      trailing: SizedBox(
                        width: 150,
                        child: Row(
                          children: [
                            IconButton(
                              icon: Icon(Icons.favorite),
                              onPressed: () {
                                FirebaseFirestore.instance
                                    .collection('recipes')
                                    .doc(recipe.id)
                                    .update({'isFavorite' : false});
                              },
                            ),
                            IconButton(
                                icon: Icon(Icons.edit),
                                onPressed: () =>
                                    _displayAddDialog(context)
                            ),
                            IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () =>
                                    _deleteRecipe(recipe.id)
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        var recipeId = recipe.id;
                        Navigator.push(context, MaterialPageRoute(builder: (context) => RecipeScreen(recipeId, ingredientName, 0)));
                      },
                    ),
                  );
                }).toList(),
              );
            }
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
          ),
          hoverColor: Theme.of(context).accentColor,
          onPressed: () {
            _createOrUpdate();
          }
      ),
      bottomNavigationBar: BottomBar(),
      // bottomNavigationBar: BottomNavBar(_currentIndex),
    );
  }

  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _cookingTimeController = new TextEditingController();
  final TextEditingController _ingredientsController = new TextEditingController();
  final TextEditingController _cookingProcedureController = new TextEditingController();

  Future<void> _createOrUpdate([DocumentSnapshot documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _nameController.text = documentSnapshot['name'];
      _cookingTimeController.text = documentSnapshot['cookingTime'];
      _ingredientsController.text = documentSnapshot['ingredients'];
      _cookingProcedureController.text = documentSnapshot['cookingProcedure'];
    }

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                    controller: _nameController,
                    decoration: InputDecoration(labelText: 'Name'),
                  ),
                  TextField(
                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                    controller: _cookingTimeController,
                    decoration: InputDecoration(labelText: 'Cooking time'),
                  ),
                  TextField(
                    controller: _ingredientsController,
                    decoration: InputDecoration(labelText: 'Ingredients'),
                  ),
                  TextField(
                    controller: _cookingProcedureController,
                    decoration: InputDecoration(labelText: 'Cooking procedure'),
                  ),
                  SizedBox(height: 20),
                  Center(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Theme.of(context).accentColor,
                        textStyle: Theme.of(context).textTheme.headline5,
                      ),
                      child: Text(
                        action == 'create' ? 'Create' : 'Update',
                      ),
                      onPressed: () async {
                        final String name = _nameController.text;
                        final int cookingTime = int.tryParse(_cookingTimeController.text);
                        List<String> ingredients = _ingredientsController.text.split(',');
                        final String cookingProcedure = _cookingProcedureController.text;

                        if (name != null && cookingTime != null && ingredients != null && cookingProcedure != null) {
                          if (action == 'create') {
                            await _recipes.add({'name': name, 'cookingTime': cookingTime, 'ingredients': ingredients, 'cookingProcedure': cookingProcedure});
                          }

                          if (action == 'update') {
                            await _recipes
                                .doc(documentSnapshot.id)
                                .update({'name': name, 'cookingTime': cookingTime, 'ingredients': ingredients, 'cookingProcedure': cookingProcedure});
                          }

                          _nameController.text = '';
                          _cookingTimeController.text = '';
                          _ingredientsController.text = '';
                          _cookingProcedureController.text = '';

                          Navigator.of(context).pop();
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        }
    );
  }

  Future<void> _deleteRecipe(String recipeId) async {
    await _recipes.doc(recipeId).delete();

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text('You have successfully deleted recipe.')),
    );
  }

  Future<void> _displayAddDialog(BuildContext context) {
    TextEditingController addController = new TextEditingController();
    CollectionReference ingredients = FirebaseFirestore.instance.collection('ingredients');

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: addController,
                    decoration: InputDecoration(hintText: 'Any new ingredient?'),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Colors.pink,
                      textColor: Colors.white,
                      child: Text('CANCEL'),
                      onPressed: (){
                        //back to ingredients screen
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Color(0xFF0A0E21),
                      textColor: Colors.white,
                      child: Text('SAVE'),
                      onPressed: () {
                        ingredients.add({
                          'name': addController.text,
                        });
                        addController.clear();
                      },
                    ),
                  ),
                ],
              ),
            ],
          );
        }
    );
  }
}


