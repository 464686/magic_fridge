import 'package:flutter/material.dart';
import 'package:magic_fridge/screens/favorites_screen.dart';
import 'package:magic_fridge/screens/friends_screen.dart';
import 'package:magic_fridge/screens/ingredients_screen.dart';
import 'package:magic_fridge/screens/settings_screen.dart';
import 'package:magic_fridge/screens/shopping_list_screen.dart';
import 'package:magic_fridge/widgets/bottom_bar.dart';
import 'package:magic_fridge/widgets/menu_button.dart';
import 'package:magic_fridge/widgets/top_app_bar.dart';

class HomeScreen extends StatelessWidget {

  // final uid;
  // HomeScreen(this.uid, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopAppBar('Magic Fridge'),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MenuButton(Icons.home, 'Home', HomeScreen()),
              MenuButton(Icons.list, 'Ingredients', IngredientsScreen()),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MenuButton(Icons.favorite, 'Favorites', FavoriteRecipesScreen(0)),
              MenuButton(Icons.list, 'Shopping List', ShoppingListScreen()),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // MenuButton(Icons.people, 'Friends', FriendsScreen()),
              MenuButton(Icons.settings, 'Settings', SettingsScreen()),
            ],
          ),
        ],
      ),
      bottomNavigationBar: BottomBar(),
      // bottomNavigationBar: BottomNavBar(_currentIndex),
    );
  }
}
