import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:magic_fridge/screens/recipes_screen.dart';
import 'package:magic_fridge/widgets/bottom_bar.dart';
import 'package:magic_fridge/widgets/top_app_bar.dart';

class IngredientsScreen extends StatefulWidget {
  static const String id = 'ingredients_screen';

  // final uid;
  // IngredientsScreen(this.uid);

  @override
  _IngredientsScreenState createState() => _IngredientsScreenState();
}

class _IngredientsScreenState extends State<IngredientsScreen> {
  final TextEditingController _nameController = TextEditingController();

  CollectionReference ingredients = FirebaseFirestore.instance.collection('ingredients');

  FirebaseAuth _firebaseAuth;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopAppBar('My Ingredients'),
      body: Center(
        child: StreamBuilder(
          stream: ingredients.orderBy('name').snapshots(),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: Text('Loading'),
              );
            }
            return ListView(
              children: snapshot.data.docs.map((ingredient) {
                return Card(
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ListTile(
                    title: Text(ingredient['name']),
                    trailing: SizedBox(
                      width: 145,
                      child: Row(
                        children: [
                          IconButton(
                            icon: Icon(Icons.remove_red_eye_outlined),
                            onPressed: () async {
                              var ingredientName = ingredient['name'];
                              var ingredientId = ingredient.id;
                              Navigator.push(context, MaterialPageRoute(builder: (context) => RecipesScreen(ingredientName, ingredientId)));
                            },
                          ),
                          IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () =>
                                  _displayAddDialog(context)
                          ),
                          IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () =>
                                  _deleteIngredient(ingredient.id)
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
        ),
        hoverColor: Theme.of(context).accentColor,
        onPressed: () {
          _createOrUpdate();
        },
      ),
      bottomNavigationBar: BottomBar(),
      // bottomNavigationBar: BottomNavBar(_currentIndex),
    );
  }

  Future<void> _createOrUpdate([DocumentSnapshot snapshot]) async {
    String action = 'create';
    if (snapshot != null) {
      action = 'update';
      _nameController.text = snapshot['name'];
    }

    _displayAddDialog(context);
  }

  Future<void> _deleteIngredient(String ingredientId) async {
    await ingredients.doc(ingredientId).delete();

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text('Ingredient has been successfully deleted.')),
    );
  }

  Future<void> _displayAddDialog(BuildContext context) {
    TextEditingController addController = new TextEditingController();
    CollectionReference ingredients = FirebaseFirestore.instance.collection('ingredients');

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              // key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: addController,
                    decoration: InputDecoration(hintText: 'Any new ingredient?'),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Colors.pink,
                      textColor: Colors.white,
                      child: Text('CANCEL'),
                      onPressed: (){
                        //back to ingredients screen
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Color(0xFF0A0E21),
                      textColor: Colors.white,
                      child: Text('SAVE'),
                      onPressed: () {
                        ingredients.add({
                          'name': addController.text,
                        });
                        addController.clear();
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ],
          );
        }
    );
  }

  currentUser() {
    final User user = _firebaseAuth.currentUser;
    final uid = user.uid.toString();
    return uid;
  }
}
