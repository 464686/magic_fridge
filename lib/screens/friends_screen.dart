import 'package:flutter/material.dart';
import 'package:magic_fridge/widgets/bottom_bar.dart';
import 'package:magic_fridge/widgets/friend_card.dart';
import 'package:magic_fridge/widgets/top_app_bar.dart';

class FriendsScreen extends StatefulWidget {
  static const String id = 'friends_screen';

  @override
  _FriendsScreenState createState() => _FriendsScreenState();
}

class _FriendsScreenState extends State<FriendsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopAppBar('My Friends'),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FriendCard('Gabriela'),
              FriendCard('Milan'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FriendCard('Lucia'),
              FriendCard('Matus'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FriendCard('Miriam'),
              FriendCard('Jakub'),
            ],
          ),
        ],
      ),
      bottomNavigationBar: BottomBar(),
    );
  }
}