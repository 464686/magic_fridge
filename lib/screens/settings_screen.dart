import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:magic_fridge/providers/theme_provider.dart';
import 'package:magic_fridge/screens/login_screen.dart';
import 'package:magic_fridge/widgets/bottom_bar.dart';
import 'package:magic_fridge/widgets/top_app_bar.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  static const String id = "SettingsScreen";

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  int _currentIndex = 5;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopAppBar('Settings'),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Consumer<ThemeNotifier>(
              builder: (context, notifier, child) => SwitchListTile(
                title: Text(
                  'Switch Theme',
                  style: Theme.of(context).textTheme.headline5,
                ),
                onChanged: (value) {
                  notifier.toggleTheme();
                },
                value: notifier.darkTheme,
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.logout,
        ),
        hoverColor: Theme.of(context).accentColor,
        onPressed: () async {
          await FirebaseAuth.instance.signOut();

          await FacebookAuth.i.logOut();

          Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoginScreen()));
        },
      ),
      bottomNavigationBar: BottomBar(),
      // bottomNavigationBar: BottomNavBar(_currentIndex),
    );
  }
}