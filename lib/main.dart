import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:magic_fridge/providers/theme_provider.dart';
import 'package:magic_fridge/screens/login_screen.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MagicFridge());
}

class MagicFridge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ThemeNotifier(),
      child: Consumer<ThemeNotifier>(
        builder: (context, ThemeNotifier notifier, child) {
          return MaterialApp(
            title: 'Magic Fridge',
            theme: notifier.darkTheme ? dark : light,
            home: LoginScreen(),
          );
        },
      ),
    );
  }
}