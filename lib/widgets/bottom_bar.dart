import 'package:flutter/material.dart';
import 'package:magic_fridge/screens/favorites_screen.dart';
import 'package:magic_fridge/screens/friends_screen.dart';
import 'package:magic_fridge/screens/home_screen.dart';
import 'package:magic_fridge/screens/ingredients_screen.dart';
import 'package:magic_fridge/screens/settings_screen.dart';
import 'package:magic_fridge/screens/shopping_list_screen.dart';

import 'bottom_bar_button.dart';

class BottomBar extends StatelessWidget implements PreferredSizeWidget {

  BottomBar();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 5),
      decoration: BoxDecoration(
        color: Theme.of(context).bottomAppBarTheme.color,
        border: Border(
          top: BorderSide(),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: FittedBox(
              alignment: Alignment.center,
              fit: BoxFit.scaleDown,
              child: Row(
                children: [
                  BottomBarButton(Icons.home, 'Home', HomeScreen()),
                  // BottomBarButton(Icons.people, 'Friends', FriendsScreen()),
                  BottomBarButton(Icons.list, 'Ingredients', IngredientsScreen()),
                  BottomBarButton(Icons.favorite, 'Favorites', FavoriteRecipesScreen("")),
                  BottomBarButton(Icons.list, 'Shopping List', ShoppingListScreen()),
                  BottomBarButton(Icons.settings, 'Settings', SettingsScreen()),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(10);
}