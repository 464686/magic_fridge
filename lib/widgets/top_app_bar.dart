import 'package:flutter/material.dart';

class TopAppBar extends StatelessWidget implements PreferredSizeWidget {

  TopAppBar(this.title);

  String title;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Theme.of(context).appBarTheme.backgroundColor,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
              alignment: Alignment.centerLeft,
              icon: Icon(
                Icons.arrow_back,
                color: Theme.of(context).primaryIconTheme.color,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            Expanded(
              child: Container(
                child: Text(
                  title,
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
            ),
            IconButton(
                icon: Icon(
                  Icons.search,
                  color: Theme.of(context).primaryIconTheme.color,
                ),
                onPressed: () {
                  _displaySearchDialog(context);
                }
            ),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(50);

  String name;

  Future<void> _displaySearchDialog(BuildContext context) {
    TextEditingController _searchController = new TextEditingController();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: _searchController,
                    onChanged: (value) {
                      name = value;
                    },
                    decoration: InputDecoration(hintText: 'What are you looking for?'),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Colors.pink,
                      textColor: Colors.white,
                      child: Text('CANCEL'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(5),
                    child: FlatButton(
                      color: Color(0xFF0A0E21),
                      textColor: Colors.white,
                      child: Text('SEARCH'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ],
          );
        }
    );
  }
}















