import 'package:flutter/material.dart';

class BottomBarButton extends StatelessWidget {
  BottomBarButton(this.next, this.title, this.page);

  final IconData next;
  final String title;
  final Widget page;

  Widget build(BuildContext context) {
    return FlatButton(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                next,
                size: 50,
                color: Theme.of(context).primaryIconTheme.color,
              ),
              SizedBox(height: 5),
              Text(
                title,
                style: Theme.of(context).textTheme.headline1,
              ),
            ],
          ),
        ),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => page));
        }
    );
  }
}

