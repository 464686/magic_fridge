import 'package:flutter/material.dart';

class FriendCard extends StatelessWidget {
  FriendCard(this.name);

  final String name;

  Widget build(BuildContext context) {
    return FlatButton(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ClipOval(
                child: Image.asset(
                  'images/logo_size.jpg',
                  width: 100,
                  height: 100,
                  fit: BoxFit.cover,
                ),
              ),
              Text(
                name,
                style: Theme.of(context).textTheme.headline5,
              ),
            ],
          ),
        ),
        onPressed: () {
          // Navigator.push(context, MaterialPageRoute(builder: (context) => page));
        }
    );
  }
}


class Choice {
  const Choice({this.title, this.icon});
  final String title;
  final IconData icon;
}
