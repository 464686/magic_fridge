import 'package:flutter/material.dart';

class MenuButton extends StatelessWidget {
  MenuButton(this.next, this.title, this.page);

  final IconData next;
  final String title;
  final Widget page;

  Widget build(BuildContext context) {
    return FlatButton(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                next,
                size: 40,
                color: Theme.of(context).accentIconTheme.color,
              ),
              Text(
                title,
                // style: Theme.of(context).textTheme.bodyText2,
                style: Theme.of(context).textTheme.headline5,
              ),
            ],
          ),
        ),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => page));
        }
    );
  }
}