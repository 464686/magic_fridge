import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

ThemeData light = ThemeData(
  brightness: Brightness.light,
  primaryColor: Color(0xfff1ece8),
  accentColor: Color(0xff031C3D),
  scaffoldBackgroundColor: Color(0xfff1ece8),
  iconTheme: IconThemeData(
    // color: Color(0x4e6077),
    color: Color(0xff031C3D),
  ),
  textTheme: TextTheme(
    headline6: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff031C3D)),
    headline1: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xfff1ece8)),
    headline2: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xfff1ece8)),
    headline3: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.pink),
    headline5: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff031C3D)),
    bodyText1: TextStyle(fontSize: 18, fontWeight: FontWeight.w300, color: Color(0xff031C3D)),
    bodyText2: TextStyle(fontSize: 18, fontWeight: FontWeight.w300, color: Color(0xff031C3D)),
  ),
  appBarTheme: AppBarTheme(
    backgroundColor: Color(0xff031C3D),
    textTheme: TextTheme(
      headline5: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xfff1ece8)),
    ),
  ),
  cardTheme: CardTheme(
    color: Color(0xffa5a09c),
  ),
  buttonTheme: ButtonThemeData(
    buttonColor: Color(0xff031C3D),
  ),
  primaryIconTheme: IconThemeData(
    color: Color(0xfff1ece8),
  ),
  bottomAppBarTheme: BottomAppBarTheme(
    color: Color(0xff031C3D),
  ),
  accentIconTheme: IconThemeData(
    color: Color(0xff031C3D),
  ),
  dialogBackgroundColor: Color(0xffa5a09c),
);

ThemeData dark = ThemeData(
  brightness: Brightness.dark,
  primaryColor: Color(0xff031C3D),
  accentColor: Color(0xff1d3657),
  scaffoldBackgroundColor: Color(0xff50698a),
  iconTheme: IconThemeData(
    color: Color(0xfff1ece8),
  ),
  textTheme: TextTheme(
    headline6: TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.bold,
        color: Color(0xff364f70),
    ),
    headline3: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.pink),
    headline5: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff031c3d)),
    headline2: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xfff1ece8)),
    headline1: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xfff1ece8)),
    bodyText1: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Color(0xff031c3d)),
    bodyText2: TextStyle(fontSize: 15, fontWeight: FontWeight.w500, color: Color(0xfff1ece8)),
  ),
  appBarTheme: AppBarTheme(
    backgroundColor: Color(0xff031C3D),
  ),
  buttonTheme: ButtonThemeData(
    buttonColor: Color(0xFF0A0E21),
  ),
  cardTheme: CardTheme(
    color: Color(0xff364f70),
  ),
  primaryIconTheme: IconThemeData(
    color: Color(0xfff1ece8),
  ),
  accentIconTheme: IconThemeData(
    color: Color(0xff031c3d),
  ),
  bottomAppBarTheme: BottomAppBarTheme(
    color: Color(0xff031C3D),
  ),
  dialogBackgroundColor: Color(0xff50698a),
);

class ThemeNotifier extends ChangeNotifier {
  final String key = 'theme';
  SharedPreferences _pref;
  bool _darkTheme;

  bool get darkTheme => _darkTheme;

  ThemeNotifier() {
    _darkTheme = true;
    _loadFromPrefs();
  }

  toggleTheme() {
    _darkTheme = !_darkTheme;
    _saveToPrefs();
    notifyListeners();
  }

  _initPrefs() async {
    if (_pref == null) {
      _pref = await SharedPreferences.getInstance();
    }
  }

  _loadFromPrefs() async {
    await _initPrefs();
    _darkTheme = _pref.getBool(key) ?? true;
    notifyListeners();
  }

  _saveToPrefs() async {
    await  _initPrefs();
    _pref.setBool(key, _darkTheme);
  }
}